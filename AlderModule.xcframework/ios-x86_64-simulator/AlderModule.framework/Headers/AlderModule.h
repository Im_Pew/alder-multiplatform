#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class AlderModuleKotlinEnumCompanion, AlderModuleKotlinEnum<E>, AlderModuleAlderFiles, AlderModuleKotlinArray<T>, AlderModuleAlderUtil, NSObject, AlderModuleConfigCompanion, AlderModuleConfig, AlderModuleConfigFileCompanion, AlderModuleConfigFile, AlderModuleAlderPlatformUtil, AlderModuleFilePlatformManager, AlderModuleAlder, AlderModuleAlderPreferences, AlderModuleKotlinThrowable, AlderModuleKotlinException, AlderModuleKotlinRuntimeException, AlderModuleKotlinIllegalStateException, AlderModuleKotlinx_serialization_coreSerializersModule, AlderModuleKotlinx_serialization_coreSerialKind, AlderModuleKotlinNothing;

@protocol AlderModuleKotlinComparable, AlderModuleKotlinx_serialization_coreKSerializer, AlderModuleCategory, AlderModuleKotlinIterator, AlderModuleKotlinx_serialization_coreEncoder, AlderModuleKotlinx_serialization_coreSerialDescriptor, AlderModuleKotlinx_serialization_coreSerializationStrategy, AlderModuleKotlinx_serialization_coreDecoder, AlderModuleKotlinx_serialization_coreDeserializationStrategy, AlderModuleKotlinx_serialization_coreCompositeEncoder, AlderModuleKotlinAnnotation, AlderModuleKotlinx_serialization_coreCompositeDecoder, AlderModuleKotlinx_serialization_coreSerializersModuleCollector, AlderModuleKotlinKClass, AlderModuleKotlinKDeclarationContainer, AlderModuleKotlinKAnnotatedElement, AlderModuleKotlinKClassifier;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface AlderModuleBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface AlderModuleBase (AlderModuleBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface AlderModuleMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface AlderModuleMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorAlderModuleKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface AlderModuleNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface AlderModuleByte : AlderModuleNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface AlderModuleUByte : AlderModuleNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface AlderModuleShort : AlderModuleNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface AlderModuleUShort : AlderModuleNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface AlderModuleInt : AlderModuleNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface AlderModuleUInt : AlderModuleNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface AlderModuleLong : AlderModuleNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface AlderModuleULong : AlderModuleNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface AlderModuleFloat : AlderModuleNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface AlderModuleDouble : AlderModuleNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface AlderModuleBoolean : AlderModuleNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol AlderModuleKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface AlderModuleKotlinEnum<E> : AlderModuleBase <AlderModuleKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) AlderModuleKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AlderFiles")))
@interface AlderModuleAlderFiles : AlderModuleKotlinEnum<AlderModuleAlderFiles *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) AlderModuleAlderFiles *somethingNew __attribute__((swift_name("somethingNew")));
@property (class, readonly) AlderModuleAlderFiles *transactions __attribute__((swift_name("transactions")));
@property (class, readonly) AlderModuleAlderFiles *nestoNowo __attribute__((swift_name("nestoNowo")));
+ (AlderModuleKotlinArray<AlderModuleAlderFiles *> *)values __attribute__((swift_name("values()")));
- (NSString *)getLogFileName __attribute__((swift_name("getLogFileName()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AlderUtil")))
@interface AlderModuleAlderUtil : AlderModuleBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)alderUtil __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) AlderModuleAlderUtil *shared __attribute__((swift_name("shared")));
- (NSString *)currentTime __attribute__((swift_name("currentTime()")));
- (NSString *)getDeviceIdContext:(NSObject *)context __attribute__((swift_name("getDeviceId(context:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)isAppAliveContext:(NSObject *)context completionHandler:(void (^)(AlderModuleBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("isAppAlive(context:completionHandler:)")));
@end;

__attribute__((swift_name("Category")))
@protocol AlderModuleCategory
@required
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Greeting")))
@interface AlderModuleGreeting : AlderModuleBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)greeting __attribute__((swift_name("greeting()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Platform")))
@interface AlderModulePlatform : AlderModuleBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (readonly) NSString *platform __attribute__((swift_name("platform")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Config")))
@interface AlderModuleConfig : AlderModuleBase
- (instancetype)initWithSecretKey:(NSString *)secretKey authUrl:(NSString *)authUrl cacheSize:(int32_t)cacheSize cacheTime:(int32_t)cacheTime __attribute__((swift_name("init(secretKey:authUrl:cacheSize:cacheTime:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) AlderModuleConfigCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (AlderModuleConfig *)doCopySecretKey:(NSString *)secretKey authUrl:(NSString *)authUrl cacheSize:(int32_t)cacheSize cacheTime:(int32_t)cacheTime __attribute__((swift_name("doCopy(secretKey:authUrl:cacheSize:cacheTime:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *authUrl __attribute__((swift_name("authUrl")));
@property (readonly) int32_t cacheSize __attribute__((swift_name("cacheSize")));
@property (readonly) int32_t cacheTime __attribute__((swift_name("cacheTime")));
@property (readonly) NSString *secretKey __attribute__((swift_name("secretKey")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Config.Companion")))
@interface AlderModuleConfigCompanion : AlderModuleBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) AlderModuleConfigCompanion *shared __attribute__((swift_name("shared")));
- (id<AlderModuleKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConfigFile")))
@interface AlderModuleConfigFile : AlderModuleBase
- (instancetype)initWithConfig:(AlderModuleConfig *)config regex:(NSArray<NSString *> *)regex __attribute__((swift_name("init(config:regex:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) AlderModuleConfigFileCompanion *companion __attribute__((swift_name("companion")));
- (AlderModuleConfig *)component1 __attribute__((swift_name("component1()")));
- (NSArray<NSString *> *)component2 __attribute__((swift_name("component2()")));
- (AlderModuleConfigFile *)doCopyConfig:(AlderModuleConfig *)config regex:(NSArray<NSString *> *)regex __attribute__((swift_name("doCopy(config:regex:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) AlderModuleConfig *config __attribute__((swift_name("config")));
@property (readonly) NSArray<NSString *> *regex __attribute__((swift_name("regex")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConfigFile.Companion")))
@interface AlderModuleConfigFileCompanion : AlderModuleBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) AlderModuleConfigFileCompanion *shared __attribute__((swift_name("shared")));
- (id<AlderModuleKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AlderPlatformUtil")))
@interface AlderModuleAlderPlatformUtil : AlderModuleBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)alderPlatformUtil __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) AlderModuleAlderPlatformUtil *shared __attribute__((swift_name("shared")));
- (NSString *)currentTimePattern:(NSString *)pattern __attribute__((swift_name("currentTime(pattern:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)isAppAliveContext:(NSObject *)context completionHandler:(void (^)(AlderModuleBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("isAppAlive(context:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)isInternetConnectedContext:(NSObject *)context completionHandler:(void (^)(AlderModuleBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("isInternetConnected(context:completionHandler:)")));
- (NSString *)retrieveDeviceIdContext:(NSObject *)context __attribute__((swift_name("retrieveDeviceId(context:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FilePlatformManager")))
@interface AlderModuleFilePlatformManager : AlderModuleBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)filePlatformManager __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) AlderModuleFilePlatformManager *shared __attribute__((swift_name("shared")));
- (void)appendToFileContext:(NSObject *)context filename:(NSString *)filename content:(NSString *)content __attribute__((swift_name("appendToFile(context:filename:content:)")));
- (void)clearFileContext:(NSObject *)context filename:(NSString *)filename __attribute__((swift_name("clearFile(context:filename:)")));
- (void)clearUnusedFilesContext:(NSObject *)context __attribute__((swift_name("clearUnusedFiles(context:)")));
- (int64_t)fileSizeContext:(NSObject *)context filename:(NSString *)filename __attribute__((swift_name("fileSize(context:filename:)")));
- (NSString *)getFileContentContext:(NSObject *)context filename:(NSString *)filename __attribute__((swift_name("getFileContent(context:filename:)")));
- (void)removeFileContext:(NSObject *)context filename:(NSString *)filename __attribute__((swift_name("removeFile(context:filename:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Alder")))
@interface AlderModuleAlder : AlderModuleBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)alder __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) AlderModuleAlder *shared __attribute__((swift_name("shared")));
- (NSString *)getIds __attribute__((swift_name("getIds()")));
- (void)doInitContext:(NSObject *)context rawJson:(NSString *)rawJson __attribute__((swift_name("doInit(context:rawJson:)")));
- (void)logTag:(NSString *)tag log:(NSString *)log category:(id<AlderModuleCategory> _Nullable)category __attribute__((swift_name("log(tag:log:category:)")));
- (void)loginUserUserId:(NSString *)userId __attribute__((swift_name("loginUser(userId:)")));
- (void)logoutUser __attribute__((swift_name("logoutUser()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AlderPreferences")))
@interface AlderModuleAlderPreferences : AlderModuleBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)alderPreferences __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) AlderModuleAlderPreferences *shared __attribute__((swift_name("shared")));
- (NSDictionary<NSString *, id> *)getAllCategoriesContext:(NSObject *)context __attribute__((swift_name("getAllCategories(context:)")));
- (NSString * _Nullable)getDeviceIdContext:(NSObject *)context __attribute__((swift_name("getDeviceId(context:)")));
- (NSString * _Nullable)getLinkContext:(NSObject *)context category:(id<AlderModuleCategory> _Nullable)category __attribute__((swift_name("getLink(context:category:)")));
- (NSString * _Nullable)getUserIdContext:(NSObject *)context __attribute__((swift_name("getUserId(context:)")));
- (void)removeLinkContext:(NSObject *)context category:(NSString *)category userId:(NSString * _Nullable)userId __attribute__((swift_name("removeLink(context:category:userId:)")));
- (void)removeLinksContext:(NSObject *)context userId:(NSString * _Nullable)userId __attribute__((swift_name("removeLinks(context:userId:)")));
- (void)removeUserContext:(NSObject *)context __attribute__((swift_name("removeUser(context:)")));
- (void)saveDeviceIdContext:(NSObject *)context deviceId:(NSString *)deviceId __attribute__((swift_name("saveDeviceId(context:deviceId:)")));
- (void)saveLinkContext:(NSObject *)context url:(NSString *)url alderCategory:(id<AlderModuleCategory> _Nullable)alderCategory __attribute__((swift_name("saveLink(context:url:alderCategory:)")));
- (void)saveUserIdContext:(NSObject *)context userId:(NSString *)userId __attribute__((swift_name("saveUserId(context:userId:)")));
@property (readonly) NSString *DEVICE_ID __attribute__((swift_name("DEVICE_ID")));
@property (readonly) NSString *LINKS_PREFERENCES __attribute__((swift_name("LINKS_PREFERENCES")));
@property (readonly) NSString *USER_ID __attribute__((swift_name("USER_ID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface AlderModuleKotlinEnumCompanion : AlderModuleBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) AlderModuleKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface AlderModuleKotlinArray<T> : AlderModuleBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(AlderModuleInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<AlderModuleKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface AlderModuleKotlinThrowable : AlderModuleBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (AlderModuleKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) AlderModuleKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end;

__attribute__((swift_name("KotlinException")))
@interface AlderModuleKotlinException : AlderModuleKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinRuntimeException")))
@interface AlderModuleKotlinRuntimeException : AlderModuleKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinIllegalStateException")))
@interface AlderModuleKotlinIllegalStateException : AlderModuleKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinCancellationException")))
@interface AlderModuleKotlinCancellationException : AlderModuleKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(AlderModuleKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol AlderModuleKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<AlderModuleKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<AlderModuleKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol AlderModuleKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<AlderModuleKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<AlderModuleKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol AlderModuleKotlinx_serialization_coreKSerializer <AlderModuleKotlinx_serialization_coreSerializationStrategy, AlderModuleKotlinx_serialization_coreDeserializationStrategy>
@required
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol AlderModuleKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol AlderModuleKotlinx_serialization_coreEncoder
@required
- (id<AlderModuleKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<AlderModuleKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<AlderModuleKotlinx_serialization_coreEncoder>)encodeInlineInlineDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("encodeInline(inlineDescriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<AlderModuleKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<AlderModuleKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) AlderModuleKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol AlderModuleKotlinx_serialization_coreSerialDescriptor
@required
- (NSArray<id<AlderModuleKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<AlderModuleKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) AlderModuleKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol AlderModuleKotlinx_serialization_coreDecoder
@required
- (id<AlderModuleKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<AlderModuleKotlinx_serialization_coreDecoder>)decodeInlineInlineDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("decodeInline(inlineDescriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (AlderModuleKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<AlderModuleKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<AlderModuleKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) AlderModuleKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol AlderModuleKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<AlderModuleKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNullableSerializableElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<AlderModuleKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<AlderModuleKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) AlderModuleKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface AlderModuleKotlinx_serialization_coreSerializersModule : AlderModuleBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)dumpToCollector:(id<AlderModuleKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<AlderModuleKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<AlderModuleKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<AlderModuleKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));
- (id<AlderModuleKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<AlderModuleKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<AlderModuleKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<AlderModuleKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol AlderModuleKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface AlderModuleKotlinx_serialization_coreSerialKind : AlderModuleBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol AlderModuleKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<AlderModuleKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<AlderModuleKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<AlderModuleKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<AlderModuleKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) AlderModuleKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface AlderModuleKotlinNothing : AlderModuleBase
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol AlderModuleKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<AlderModuleKotlinKClass>)kClass provider:(id<AlderModuleKotlinx_serialization_coreKSerializer> (^)(NSArray<id<AlderModuleKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<AlderModuleKotlinKClass>)kClass serializer:(id<AlderModuleKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<AlderModuleKotlinKClass>)baseClass actualClass:(id<AlderModuleKotlinKClass>)actualClass actualSerializer:(id<AlderModuleKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<AlderModuleKotlinKClass>)baseClass defaultDeserializerProvider:(id<AlderModuleKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultDeserializerBaseClass:(id<AlderModuleKotlinKClass>)baseClass defaultDeserializerProvider:(id<AlderModuleKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefaultDeserializer(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultSerializerBaseClass:(id<AlderModuleKotlinKClass>)baseClass defaultSerializerProvider:(id<AlderModuleKotlinx_serialization_coreSerializationStrategy> _Nullable (^)(id))defaultSerializerProvider __attribute__((swift_name("polymorphicDefaultSerializer(baseClass:defaultSerializerProvider:)")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol AlderModuleKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol AlderModuleKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol AlderModuleKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol AlderModuleKotlinKClass <AlderModuleKotlinKDeclarationContainer, AlderModuleKotlinKAnnotatedElement, AlderModuleKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
