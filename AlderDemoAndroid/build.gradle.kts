plugins {
    id("com.android.application")
    kotlin("android")
}

dependencies {
    implementation(project(":AlderModule"))
    implementation("com.google.android.material:material:1.4.0")
    implementation("androidx.appcompat:appcompat:1.4.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.2")
    implementation ("androidx.multidex:multidex:2.0.1")
}

android {
    compileSdkVersion(31)
    defaultConfig {
        applicationId = "hr.sevenofnine.multiplatform.alder.android"
        minSdkVersion(16)
        targetSdkVersion(31)
        versionCode = 1
        versionName = "1.0"
        multiDexEnabled = true
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
}