package hr.sevenofnine.multiplatform.alder.android

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import hr.sevenofnine.multiplatform.alder.AlderUtil
import hr.sevenofnine.multiplatform.alder.alder.Alder
import hr.sevenofnine.multiplatform.alder.log.AlderPreferences

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()

        val tv: TextView = findViewById(R.id.text_view)
        val testLogButton: Button = findViewById(R.id.testLog)
        val deviceId = AlderUtil.getDeviceId(application)

        testLogButton.setOnClickListener { sendLog() }

        AlderPreferences.saveDeviceId(application, deviceId)
        tv.text = AlderPreferences.getDeviceId(application)

        val readBytes = String(resources.openRawResource(R.raw.config).readBytes())
        Alder.init(application, readBytes)

        Alder.log(TAG, "Hello Android multiplatform")
        Alder.log(TAG, "New line")
//        Alder.loginUser("loginUser")
        Alder.log(TAG, "New log")
//        Alder.logoutUser()
    }

    private fun sendLog() {
        Alder.log(TAG, "Button activated log")
    }
}
