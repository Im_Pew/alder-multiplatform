Pod::Spec.new do |spec|
    spec.name                     = 'AlderModule'
    spec.version                  = '0.0.2'
    spec.homepage                 = 'https://www.cocoapods.org'
    spec.source                   = { :git => "git@gitlab.com:Im_Pew/alder-multiplatform.git", :tag => "test" } #:tag => spec.version, :branch => "test" } #"#{spec.version}"
    spec.authors                  = 'Danijel Turic'
    spec.license                  = { :type => "MIT", :file => "LICENSE" }
    spec.homepage 		          = 'https://www.cocoapods.org'
    spec.summary                  = 'Some description for the Shared Module'

    spec.static_framework         = true
    spec.vendored_frameworks      = "AlderModule.xcframework"
    spec.libraries                = "c++"
    spec.module_name              = "#{spec.name}_umbrella"

    spec.ios.deployment_target = '12'

    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
                

#    spec.pod_target_xcconfig = {
#        'KOTLIN_PROJECT_PATH' => ':AlderModule',
#        'PRODUCT_MODULE_NAME' => 'AlderModule',
#    }

#     spec.script_phases = [
#         {
#             :name => 'Build AlderModule',
#             :execution_position => :before_compile,
#             :shell_path => '/bin/sh',
#             :script => <<-SCRIPT
#                 if [ "YES" = "$COCOAPODS_SKIP_KOTLIN_BUILD" ]; then
#                   echo "Skipping Gradle build task invocation due to COCOAPODS_SKIP_KOTLIN_BUILD environment variable set to \"YES\""
#                   exit 0
#                 fi
#                 set -ev
#                 REPO_ROOT="$PODS_TARGET_SRCROOT"
#                 "$REPO_ROOT/../gradlew" -p "$REPO_ROOT" $KOTLIN_PROJECT_PATH:syncFramework \
#                     -Pkotlin.native.cocoapods.platform=$PLATFORM_NAME \
#                     -Pkotlin.native.cocoapods.archs="$ARCHS" \
#                     -Pkotlin.native.cocoapods.configuration=$CONFIGURATION
#             SCRIPT
#         }
#     ]
end