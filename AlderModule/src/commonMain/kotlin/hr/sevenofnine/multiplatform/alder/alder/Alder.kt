package hr.sevenofnine.multiplatform.alder.alder

import hr.sevenofnine.multiplatform.alder.AlderUtil
import hr.sevenofnine.multiplatform.alder.Category
import hr.sevenofnine.multiplatform.alder.log.AlderCacheManager
import hr.sevenofnine.multiplatform.alder.log.AlderPreferences
import hr.sevenofnine.multiplatform.alder.log.AlderUrlUtil
import hr.sevenofnine.multiplatform.alder.platform.AlderPlatformUtil.retrieveDeviceId
import hr.sevenofnine.multiplatform.alder.platform.ApplicationContext
import hr.sevenofnine.multiplatform.alder.platform.FilePlatformManager
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.utils.io.concurrent.*
import io.ktor.utils.io.core.internal.*
import kotlinx.coroutines.*

@DangerousInternalIoApi
object Alder {
    internal val scope = CoroutineScope(Dispatchers.Default + CoroutineName("Alder"))

    private var context: ApplicationContext? by shared(null)

    private val httpClient = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
        expectSuccess = false
    }

    /**
     * Call this method so Alder can be initialized and used through app
     */
    @DangerousInternalIoApi
    fun init(context: ApplicationContext, rawJson: String) {
        this.context = context
        var deviceId = AlderPreferences.getDeviceId(context)

        ConfigParser.parse(rawJson)

        AlderCacheManager.init(context)

        if (deviceId == null) {
            deviceId = retrieveDeviceId(context)
            AlderPreferences.saveDeviceId(context, deviceId)
        }
    }

    /**
     * Use this method when user is logged into app. When method is called all cached files
     * with log data that represents state without user are
     * sent to server and new ones are created, that contains userId
     *
     * @param userId String value of user ID
     */
    fun loginUser(userId: String) {
        if (context == null) throw RuntimeException("Call Alder.init(Context) first")
        clearLinks()
        AlderPreferences.saveUserId(context!!, userId)
    }

    /**
     * Use this method when user is logged out from app. When method is called all cached files
     * with log data that represents state with logged user are
     * sent to server and new ones are created, that don't contains userId
     */
    fun logoutUser() {
        if (context == null) throw RuntimeException("Call Alder.init(Context) first")
        clearLinks()
        AlderPreferences.removeUser(context!!)
    }

    private fun clearLinks() {
        val oldUserId = AlderPreferences.getUserId(context!!)
        scope.launch {
            AlderCacheManager.clearCaches()
            FilePlatformManager.clearUnusedFiles(context!!)
            AlderPreferences.removeLinks(context!!, oldUserId)
        }
    }

    private fun formatCurrentTimestamp() = AlderUtil.currentTime()

    /**
     * Call this method when you wanna send log, log is cached locally before it is sent to server
     * Log will be sent in time intervals that are configured in config file, or if log file reach
     * size that is also set in config file. If caching interval or max cache file size isn't specified
     * in config data interval is set to 60 seconds, and file size is set to 100 kB
     *
     * @param tag Source of log (example: MainActivity, LoginFragment...)
     * @param log Body of log (example: User entered wrong PIN 3 times...)
     * @param category Category into which log should be saved, if category isn't provided log is saved to general
     */
    fun log(tag: String, log: String, category: Category? = null) =
        cacheLog("${formatCurrentTimestamp()}:$tag/${AlderRegex.parseWithRegex(log)}", category)

    /**
     * Send log data to cache
     */
    internal fun cacheLog(body: String, category: Category? = null) {
        AlderCacheManager.cache(body, category)
    }

    /**
     * Method returns ids for current device and user (if user is currently logged, or * if there no logged user)
     */
    fun getIds(): String {
        if (context == null) throw RuntimeException("Call Alder.init(Context) first")
        return "${AlderPreferences.getDeviceId(context!!)} - ${
            AlderPreferences.getUserId(
                context!!
            ) ?: "*"
        }"
    }

    /**
     * Send data to server or cache it again if there is some problems
     */
    internal suspend fun sendLog(
        body: String,
        category: Category? = null,
        shouldArchive: Boolean = true
    ): Boolean {
        if (context == null) throw RuntimeException("Call Alder.init(Context) first")
        return withContext(scope.coroutineContext + Dispatchers.Default) {
            try {
                val url = AlderUrlUtil.getUrl(context!!, category)
                sendLog(url ?: throw Exception("Url is null"), body)
                true
            } catch (e: Exception) {
                if (shouldArchive) AlderCacheManager.archiveCacheFile(category)
                false
            }
        }
    }

    @Throws(Exception::class)
    private suspend fun sendLog(url: String, log: String) {
        httpClient.put<String> {
            url(url)
            body = log
        }
    }
}

