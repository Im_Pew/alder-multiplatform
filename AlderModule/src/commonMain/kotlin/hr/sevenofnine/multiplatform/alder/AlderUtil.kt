package hr.sevenofnine.multiplatform.alder

import hr.sevenofnine.multiplatform.alder.platform.AlderPlatformUtil
import hr.sevenofnine.multiplatform.alder.platform.ApplicationContext

object AlderUtil {

    fun getDeviceId(context: ApplicationContext): String {
        return AlderPlatformUtil.retrieveDeviceId(context)
    }

    suspend fun isAppAlive(context: ApplicationContext) = AlderPlatformUtil.isAppAlive(context)

    fun currentTime(): String {
        return AlderPlatformUtil.currentTime()
    }

    fun currentTimestamp() = AlderPlatformUtil.currentTimestamp()
}