package hr.sevenofnine.multiplatform.alder.alder

import hr.sevenofnine.multiplatform.alder.model.Config
import hr.sevenofnine.multiplatform.alder.model.ConfigFile
import io.ktor.utils.io.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

internal object ConfigParser {

    fun getConfigFile(json: String): ConfigFile {
        return Json.decodeFromString(json)
    }

    fun parse(json: String) {
        val configFile = Json.decodeFromString<ConfigFile>(json)

        populateAlderConfig(configFile.config)
        populateAlderRegexConfig(configFile.regex)
    }

    private fun populateAlderConfig(config: Config) {
        AlderConfig.SECRET_KEY.value = config.secretKey
        AlderConfig.AUTH_URL.value = config.authUrl
        AlderConfig.CACHE_SIZE.value = config.cacheSize
        AlderConfig.CACHE_TIME.value = config.cacheTime
    }

    private fun populateAlderRegexConfig(regex: List<String>) {
        AlderRegex.regex.value = regex
    }
}