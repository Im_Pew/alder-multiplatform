package hr.sevenofnine.multiplatform.alder.platform

expect object FilePlatformManager {

    fun getFileContent(context: ApplicationContext, filename: String): String
    fun appendToFile(context: ApplicationContext, filename: String, content: String)
    fun getAllArchivedCacheFiles(context: ApplicationContext, filename: String): List<String>
    fun removeFile(context: ApplicationContext, filename: String)
    fun renameFile(context: ApplicationContext, oldName: String, newName: String)
    fun fileSize(context: ApplicationContext, filename: String): Long
    fun clearFile(context: ApplicationContext, filename: String)
    fun clearUnusedFiles(context: ApplicationContext)
}