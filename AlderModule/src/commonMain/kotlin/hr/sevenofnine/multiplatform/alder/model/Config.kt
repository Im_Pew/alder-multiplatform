package hr.sevenofnine.multiplatform.alder.model

import kotlinx.serialization.*

@Serializable data class Config(
    @SerialName("secret_key") val secretKey: String,
    @SerialName("auth_url") val authUrl: String,
    @SerialName("cache_size") val cacheSize: Int,
    @SerialName("cache_time") val cacheTime: Int
)

@Serializable data class ConfigFile(
    val config: Config,
    val regex: List<String>
)
