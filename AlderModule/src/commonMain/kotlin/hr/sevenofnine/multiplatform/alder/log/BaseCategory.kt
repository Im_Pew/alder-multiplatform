package hr.sevenofnine.multiplatform.alder.log

import hr.sevenofnine.multiplatform.alder.Category

internal class BaseCategory(override val name: String) : Category