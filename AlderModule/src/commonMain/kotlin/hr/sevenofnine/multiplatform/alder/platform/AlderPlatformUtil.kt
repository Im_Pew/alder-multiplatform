package hr.sevenofnine.multiplatform.alder.platform


expect object AlderPlatformUtil {

    fun retrieveDeviceId(context: ApplicationContext): String
    suspend fun isAppAlive(context: ApplicationContext): Boolean
    fun currentTime(pattern: String = "HH:mm:ss.SSS dd.MM.yyyy"): String
    fun currentTimestamp(): Long
    suspend fun isInternetConnected(context: ApplicationContext): Boolean
}