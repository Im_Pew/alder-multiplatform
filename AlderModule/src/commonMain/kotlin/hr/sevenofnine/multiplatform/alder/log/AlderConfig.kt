package hr.sevenofnine.multiplatform.alder.log

import hr.sevenofnine.multiplatform.alder.Category
import hr.sevenofnine.multiplatform.alder.alder.Alder
import hr.sevenofnine.multiplatform.alder.alder.AlderConfig.AUTH_URL
import hr.sevenofnine.multiplatform.alder.alder.AlderConfig.SECRET_KEY
import hr.sevenofnine.multiplatform.alder.platform.ApplicationContext
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.core.internal.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable

@DangerousInternalIoApi
internal object AlderUrlUtil {

    const val SPLITTER = '˚'

    private val httpClient = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    @Throws(Exception::class)
    private suspend fun retrieveUrl(
        context: ApplicationContext,
        alderCategory: Category?
    ): String? {
        val deviceId =
            AlderPreferences.getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = AlderPreferences.getUserId(context)
        val category = alderCategory?.name

        return withContext(Alder.scope.coroutineContext + Dispatchers.Default) {
            val request: HttpResponse = httpClient.post {
                url(AUTH_URL.value)
                method = HttpMethod.Post
                contentType(ContentType.Application.Json)
                headers {
                    append("Authorization", SECRET_KEY.value)
                }
                body = Request(deviceId, userId, category)
            }
            if (request.status == HttpStatusCode.OK) {
                val link = request.receive<String>()
                AlderPreferences.saveLink(context, link, alderCategory)
                link
            } else {
                AlderPreferences.saveLink(context, null, alderCategory)
                null
            }
        }
    }

    @Throws(Exception::class)
    internal suspend fun getUrl(context: ApplicationContext, category: Category?): String? {
        return AlderPreferences.getLink(context, category) ?: return retrieveUrl(context, category)
    }

    internal fun parseCategory(tag: String): Category? {
        val index = 2
        val name = tag.split(SPLITTER)
        val categoryName = name[index]

        return if (categoryName == "*") null
        else BaseCategory(categoryName)
    }

    internal fun generateTag(
        deviceId: String,
        category: String = "*",
        userId: String = "*"
    ) = "${deviceId}$SPLITTER${userId}$SPLITTER${category}"

    internal fun getTagRegex(
        deviceId: String,
        userId: String? = null
    ): Regex {
        val pattern = "${deviceId}$SPLITTER${userId ?: "\\*"}$SPLITTER(\\*|[\\w.]*)"
        return Regex(pattern)
    }

    internal fun getAllCategories(context: ApplicationContext) =
        AlderPreferences.getAllCategories(context)

    @Serializable
    data class Request(
        val deviceId: String,
        val userId: String?,
        val category: String?
    )
}