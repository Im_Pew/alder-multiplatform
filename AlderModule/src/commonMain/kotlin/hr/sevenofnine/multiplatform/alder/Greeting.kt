package hr.sevenofnine.multiplatform.alder

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}