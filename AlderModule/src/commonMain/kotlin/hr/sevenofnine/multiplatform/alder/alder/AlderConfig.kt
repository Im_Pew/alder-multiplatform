package hr.sevenofnine.multiplatform.alder.alder

import kotlinx.atomicfu.AtomicInt
import kotlinx.atomicfu.AtomicRef
import kotlinx.atomicfu.atomic

internal object AlderConfig {
    public var SECRET_KEY: AtomicRef<String> = atomic("")

    public var AUTH_URL: AtomicRef<String> = atomic("")

    public var CACHE_SIZE: AtomicInt = atomic(0)

    public var CACHE_TIME: AtomicInt = atomic(0)
}
