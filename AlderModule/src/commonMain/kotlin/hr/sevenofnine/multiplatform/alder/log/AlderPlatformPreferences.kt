package hr.sevenofnine.multiplatform.alder.log

import hr.sevenofnine.multiplatform.alder.Category
import hr.sevenofnine.multiplatform.alder.platform.ApplicationContext

expect object AlderPreferences {

    actual val LINKS_PREFERENCES: String
    actual val DEVICE_ID: String
    actual val USER_ID: String

    fun saveLink(context: ApplicationContext, url: String?, alderCategory: Category? = null)
    fun getLink(context: ApplicationContext, category: Category?): String?
    fun removeLinks(context: ApplicationContext, userId: String?)
    fun removeLink(context: ApplicationContext, category: String, userId: String? = null)
    fun saveDeviceId(context: ApplicationContext, deviceId: String)
    fun removeUser(context: ApplicationContext)
    fun getDeviceId(context: ApplicationContext): String?
    fun saveUserId(context: ApplicationContext, userId: String)
    fun getUserId(context: ApplicationContext): String?
    fun getAllCategories(context: ApplicationContext): Map<String, Any?>
}