package hr.sevenofnine.multiplatform.alder.alder

import kotlinx.atomicfu.AtomicRef
import kotlinx.atomicfu.atomic

internal object AlderRegex {
    public var regex: AtomicRef<List<String>> = atomic(listOf())
    
    public fun parseWithRegex(string: String): String {
        var parsed = string
        regex.value.forEach {
            val regex = Regex("\"$it\"\\s?:\\s?\"([\\d\\w\\S\\s])\\s?(.*?)\"")
            if (parsed.contains(regex)) {
                parsed = regex.replace(parsed, "\"$it\":\"*\"")
            }
        }
        return parsed
    }
}
