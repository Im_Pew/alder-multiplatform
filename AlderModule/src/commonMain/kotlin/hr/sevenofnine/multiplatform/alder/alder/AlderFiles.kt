package hr.sevenofnine.agency.alder

public enum class AlderFiles(
  private val fileName: String
) {
  SOMETHING_NEW("something_new"),
  TRANSACTIONS("transactions"),
  NESTO_NOWO("nesto_nowo"),
  ;

  public fun getLogFileName(): String = this.fileName
}
