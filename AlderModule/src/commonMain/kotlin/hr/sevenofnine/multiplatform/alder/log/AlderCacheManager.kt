package hr.sevenofnine.multiplatform.alder.log

import hr.sevenofnine.multiplatform.alder.AlderUtil
import hr.sevenofnine.multiplatform.alder.Category
import hr.sevenofnine.multiplatform.alder.alder.Alder
import hr.sevenofnine.multiplatform.alder.alder.AlderConfig
import hr.sevenofnine.multiplatform.alder.log.AlderUrlUtil.parseCategory
import hr.sevenofnine.multiplatform.alder.platform.ApplicationContext
import hr.sevenofnine.multiplatform.alder.platform.FilePlatformManager
import io.ktor.utils.io.concurrent.*
import io.ktor.utils.io.core.internal.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@DangerousInternalIoApi
internal object AlderCacheManager {

    private var context: ApplicationContext? by shared(null)

    fun init(context: ApplicationContext) {
        this.context = context
        createUpdateCoroutine(cacheFileTime)
    }

    private val cacheFileSize: Int
        get() = AlderConfig.CACHE_SIZE.value

    private val cacheFileTime: Int
        get() = AlderConfig.CACHE_TIME.value

    fun cache(log: String, category: Category?) {
        if (context == null) return
        Alder.scope.launch(Dispatchers.Default) {
            try {
                AlderUrlUtil.getUrl(context!!, category)
            } catch (e: Throwable) {
                println(e.message)
            }
            val deviceId =
                AlderPreferences.getDeviceId(context!!)
                    ?: throw RuntimeException("DeviceId not set!!")
            val userId = AlderPreferences.getUserId(context!!) ?: "*"

            val fileName = AlderUrlUtil.generateTag(deviceId, category?.name ?: "*", userId)
            FilePlatformManager.appendToFile(context!!, fileName, log)

            if (FilePlatformManager.fileSize(context!!, fileName) >= cacheFileSize.toLong())
                sendLog(context!!)
        }
    }

    fun archiveCacheFile(category: Category?) {
        if (context == null) return
        Alder.scope.launch(Dispatchers.Default) {
            val deviceId =
                AlderPreferences.getDeviceId(context!!)
                    ?: throw RuntimeException("DeviceId not set!!")
            val userId = AlderPreferences.getUserId(context!!) ?: "*"

            val filename = AlderUrlUtil.generateTag(deviceId, category?.name ?: "*", userId)
            FilePlatformManager.renameFile(
                context!!,
                filename,
                "${filename}${AlderUrlUtil.SPLITTER}${AlderUtil.currentTimestamp()}"
            )
            FilePlatformManager.getFileContent(context!!, filename)
        }
    }

    internal suspend fun clearCaches() {
        sendLog(context!!)
    }

    private suspend fun sendLog(context: ApplicationContext) {
        val deviceId = AlderPreferences.getDeviceId(context) ?: ""
        val userId = AlderPreferences.getUserId(context) ?: "*"
        val files = AlderUrlUtil.getAllCategories(context).keys

        for (fileName in files) {
            val archivedFiles = FilePlatformManager.getAllArchivedCacheFiles(context, fileName)
            for (archivedFileName in archivedFiles) {
                val body = FilePlatformManager.getFileContent(
                    context,
                    archivedFileName
                )
                if (body.isBlank()) {
                    FilePlatformManager.removeFile(context, archivedFileName)
                } else {
                    val category = parseCategory(archivedFileName)
                    if (Alder.sendLog(body, category, fileName == archivedFileName)) {
                        FilePlatformManager.removeFile(context, archivedFileName)
                    }
                }
            }
        }

        val generalFileName = AlderUrlUtil.generateTag(deviceId, "*", userId)
        val body = FilePlatformManager.getFileContent(context, generalFileName)

        if (body.isBlank()) return
        if (Alder.sendLog(body)) {
            FilePlatformManager.removeFile(context, generalFileName)
        }
    }

    private fun createUpdateCoroutine(delay: Number) {
        Alder.scope.launch(Dispatchers.Default) {
            println("createUpdateCoroutine/Success")
            try {
                logic(delay)
            } catch (e: Exception) {
                println(e.message)
            }
        }
    }

    private suspend fun logic(delay: Number) {
        delay(delay.toLong())
        while (withContext(Dispatchers.Main) {
                isAppRunning(context ?: return@withContext false)
            }) {
            sendLog(context ?: break)
            println("Cached log data sent!")
            delay(delay.toLong())
        }
        println("coroutineUpdateCoroutine/logic/END")
    }

    private suspend fun isAppRunning(context: ApplicationContext) =
        AlderUtil.isAppAlive(context)
}