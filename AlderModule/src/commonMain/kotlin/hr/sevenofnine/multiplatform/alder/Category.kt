package hr.sevenofnine.multiplatform.alder

/**
 * Create custom Alder category using this
 * Allowed characters (\w, '.', '_')
 */
interface Category {
    val name: String
}