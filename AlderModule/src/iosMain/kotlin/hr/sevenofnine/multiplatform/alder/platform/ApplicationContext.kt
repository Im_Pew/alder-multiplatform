package hr.sevenofnine.multiplatform.alder.platform

import platform.darwin.NSObject

actual typealias ApplicationContext = NSObject