package hr.sevenofnine.multiplatform.alder.log

import hr.sevenofnine.multiplatform.alder.Category
import hr.sevenofnine.multiplatform.alder.log.AlderUrlUtil.generateTag
import hr.sevenofnine.multiplatform.alder.platform.ApplicationContext
import io.ktor.utils.io.core.internal.*
import platform.Foundation.NSUserDefaults

@DangerousInternalIoApi
actual object AlderPreferences {

    actual val LINKS_PREFERENCES: String = "hr.dturic.personal.tdkappdemoadler.shared_preferences"
    actual val DEVICE_ID: String = "hr.sevenofnine.agency.alder.DEVICE_ID"
    actual val USER_ID: String = "hr.sevenofnine.agency.alder.USER_ID"

    actual fun saveLink(context: ApplicationContext, url: String?, alderCategory: Category?) {
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = getUserId(context) ?: "*"
        val category = alderCategory?.name ?: "*"
        val tag = generateTag(deviceId, category, userId)

        NSUserDefaults.standardUserDefaults.setObject(url, tag)
    }

    actual fun getLink(context: ApplicationContext, category: Category?): String? {
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = getUserId(context) ?: "*"
        val category = category?.name ?: "*"
        val tag = generateTag(deviceId, category, userId)

        val link = NSUserDefaults.standardUserDefaults.stringForKey(tag)
        return link
    }

    actual fun removeLinks(context: ApplicationContext, userId: String?) {
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val regex = AlderUrlUtil.getTagRegex(deviceId, userId)

        NSUserDefaults.standardUserDefaults.dictionaryRepresentation().keys
            .filter { regex.matches(it as String) }
            .forEach { NSUserDefaults.standardUserDefaults.removeObjectForKey(it as String) }
    }

    actual fun removeLink(context: ApplicationContext, category: String, userId: String?) {
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = userId ?: "*"
        val key = generateTag(deviceId, category, userId)

        NSUserDefaults.standardUserDefaults.removeObjectForKey(key)
    }

    actual fun saveDeviceId(context: ApplicationContext, deviceId: String) {
        NSUserDefaults.standardUserDefaults.setObject(deviceId, DEVICE_ID)
    }

    actual fun getDeviceId(context: ApplicationContext): String? =
        NSUserDefaults.standardUserDefaults.stringForKey(DEVICE_ID)

    actual fun saveUserId(context: ApplicationContext, userId: String) {
        NSUserDefaults.standardUserDefaults.setObject(userId, USER_ID)
    }

    actual fun getUserId(context: ApplicationContext): String? =
        NSUserDefaults.standardUserDefaults.stringForKey(USER_ID)

    actual fun removeUser(context: ApplicationContext) =
        NSUserDefaults.standardUserDefaults.removeObjectForKey(USER_ID)

    actual fun getAllCategories(context: ApplicationContext): Map<String, Any?> {
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = getUserId(context)
        val regex = AlderUrlUtil.getTagRegex(deviceId, userId)

        return NSUserDefaults.standardUserDefaults
            .dictionaryRepresentation()
            .mapKeys { it.key as String }
            .filterKeys { key -> regex.matches(key) }
    }
}