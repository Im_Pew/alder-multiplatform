package hr.sevenofnine.multiplatform.alder.platform

import platform.Foundation.NSDate
import platform.Foundation.NSDateFormatter
import platform.Foundation.timeIntervalSince1970
import platform.UIKit.UIApplication
import platform.UIKit.UIApplicationState
import platform.UIKit.UIDevice

actual object AlderPlatformUtil {

    actual fun retrieveDeviceId(context: ApplicationContext): String =
        UIDevice.currentDevice.identifierForVendor?.UUIDString.toString()

    actual suspend fun isAppAlive(context: ApplicationContext) =
        UIApplication.sharedApplication().applicationState == UIApplicationState.UIApplicationStateActive

    actual fun currentTime(pattern: String): String {
        val dateFormatterPrint = NSDateFormatter()
        dateFormatterPrint.dateFormat = pattern

        return dateFormatterPrint.stringFromDate(NSDate())
    }

    actual fun currentTimestamp() = NSDate().timeIntervalSince1970.toLong()

    actual suspend fun isInternetConnected(context: ApplicationContext): Boolean {

        return true
    }
}