package hr.sevenofnine.multiplatform.alder.platform

import hr.sevenofnine.multiplatform.alder.alder.Alder
import io.ktor.utils.io.core.*
import io.ktor.utils.io.core.internal.*
import kotlinx.cinterop.addressOf
import kotlinx.cinterop.allocArrayOf
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.usePinned
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import platform.Foundation.*
import platform.posix.memcpy
import platform.posix.size_t

@DangerousInternalIoApi
actual object FilePlatformManager {
    actual fun getFileContent(
        context: ApplicationContext,
        filename: String
    ): String {
        val url = getFileUrl(filename)

        val data = NSFileManager.defaultManager.contentsAtPath(url?.path ?: return "")
        val bytes = ByteArray(data?.length?.toInt() ?: 0).apply {
            if (this.isEmpty()) return@getFileContent ""
            usePinned {
                memcpy(it.addressOf(0), data?.bytes, (data?.length ?: 0) as size_t)
            }
        }
        return bytes.decodeToString()
    }

    actual fun appendToFile(
        context: ApplicationContext,
        filename: String,
        content: String
    ) {
        Alder.scope.launch(Dispatchers.Default) {
            val url = getFileUrl(filename)
            val content = "$content\n"
            val nsData = memScoped {
                NSData.create(
                    bytes = allocArrayOf(content.toByteArray()),
                    length = content.toByteArray().size.toULong()
                )
            }

            val fileHandle = NSFileHandle.fileHandleForUpdatingAtPath(url?.path!!)!!
            fileHandle.seekToEndOfFile()
            fileHandle.writeData(nsData)
            fileHandle.closeFile()
        }
    }

    actual fun getAllArchivedCacheFiles(
        context: ApplicationContext,
        filename: String
    ): List<String> {
        val url = getFolderUrl()!!
        val directoryContent =
            NSFileManager.defaultManager.contentsOfDirectoryAtURL(url, null, 0, null)

        return directoryContent
            ?.map {
                println(NSURL.URLWithString(it.toString()))
                NSURL.URLWithString(it.toString())
            }
            ?.mapNotNull { it?.lastPathComponent?.removeSuffix(".log") }
            ?.filter { it.contains(filename) }
            ?: emptyList()
    }

    actual fun removeFile(
        context: ApplicationContext,
        filename: String
    ) {
        val url = getFileUrl(filename)
        NSFileManager.defaultManager.removeItemAtURL(url ?: return, null)
    }

    actual fun renameFile(
        context: ApplicationContext,
        oldName: String,
        newName: String
    ) {
        val oldUrl = getFileUrl(oldName)
        val newUrl = oldUrl?.URLByDeletingLastPathComponent
            ?.URLByAppendingPathComponent("$newName.log")

        println("New archived location: ${newUrl?.toString()}")
        NSFileManager.defaultManager.moveItemAtURL(oldUrl!!, newUrl!!, null)
    }

    actual fun fileSize(
        context: ApplicationContext,
        filename: String
    ): Long {
        val url = getFileUrl(filename)

        val fileHandle = NSFileHandle.fileHandleForUpdatingAtPath(url?.path!!)!!
        return fileHandle.availableData.length.toLong().also { fileHandle.closeFile() }
    }

    actual fun clearFile(
        context: ApplicationContext,
        filename: String
    ) {
        val url = getFileUrl(filename)

        ("" as NSString).writeToFile(url?.path!!, atomically = false)
    }

    private fun getFileUrl(filename: String): NSURL? {
        val logsPath = getFolderUrl()
        val fileURL = logsPath?.URLByAppendingPathComponent("$filename.log")

        if (!NSFileManager.defaultManager.fileExistsAtPath(fileURL?.path!!)) {
            NSFileManager.defaultManager.createFileAtPath(fileURL.path!!, NSData(), null)
        }
        return fileURL
    }

    private fun getFolderUrl(): NSURL? {
        val documentsPath = NSURL(
            fileURLWithPath = NSSearchPathForDirectoriesInDomains(
                NSDocumentDirectory,
                NSUserDomainMask,
                true
            )[0] as String
        )

        val logsPath = documentsPath.URLByAppendingPathComponent("alderCache")
        NSFileManager.defaultManager()
            .createDirectoryAtPath(
                logsPath?.path ?: return null,
                attributes = mutableMapOf<Any?, Any?>()
            )
        return logsPath
    }

    actual fun clearUnusedFiles(context: ApplicationContext) {
        val documentsPath = NSURL(
            fileURLWithPath = NSSearchPathForDirectoriesInDomains(
                NSDocumentDirectory,
                NSUserDomainMask,
                true
            )[0] as String
        )
        val logsPath = documentsPath.URLByAppendingPathComponent("alderCache")
        NSFileManager.defaultManager.removeItemAtURL(logsPath!!, null)
    }
}