package hr.sevenofnine.multiplatform.alder.platform

import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.util.*

actual object FilePlatformManager {

    private const val CACHE_FOLDER = "alderCache"

    actual fun getFileContent(
        context: ApplicationContext,
        filename: String
    ): String {
        val file = getCacheFile(context, filename)
        return try {
            with(file.bufferedReader()) {
                try {
                    readText()
                } catch (e: OutOfMemoryError) {
                    close()
                    val reducedContent = reduceFileContent(file)
                    clearFile(context, filename)
                    appendToFile(context, filename, reducedContent)
                    reducedContent
                } finally {
                    close()
                }
            }
        } catch (e: FileNotFoundException) {
            ""
        }
    }

    actual fun appendToFile(
        context: ApplicationContext,
        filename: String,
        content: String
    ) {
        val file = getCacheFile(context, filename)
        with(FileOutputStream(file, true)) {
            write("$content\n".toByteArray())
            flush()
            close()
        }
    }

    actual fun getAllArchivedCacheFiles(
        context: ApplicationContext,
        filename: String
    ) =
        File(context.cacheDir, CACHE_FOLDER)
            .listFiles { _, name ->
                name.contains(filename)
            }?.map {
                it.name.removeSuffix(".log")
            } ?: emptyList()

    actual fun removeFile(
        context: ApplicationContext,
        filename: String
    ) {
        val file = getCacheFile(context, filename)
        file.delete()
    }

    actual fun renameFile(
        context: ApplicationContext,
        oldName: String,
        newName: String
    ) {
        val file = getCacheFile(context, oldName)
        val newName = if (!newName.endsWith(".log", false)) "$newName.log" else newName
        val newFile = File(file.parentFile, newName)
        newFile.createNewFile()

        file.renameTo(newFile)
    }

    actual fun fileSize(context: ApplicationContext, filename: String): Long {
        val cacheFile = context.cacheDir

        return with(File(cacheFile, CACHE_FOLDER)) {
            if (!exists()) mkdir()

            val file = File(this, "$filename.log")

            if (!file.exists()) file.createNewFile()

            file.length()
        }
    }

    private fun getCacheFile(
        context: ApplicationContext,
        fileName: String
    ): File {
        val cacheDir = context.cacheDir

        return with(File(cacheDir, CACHE_FOLDER)) {
            if (!exists()) mkdir()

            val cacheFile = File(this, "$fileName.log")
            if (!cacheFile.exists()) cacheFile.createNewFile()
            cacheFile
        }
    }

    actual fun clearFile(
        context: ApplicationContext,
        filename: String
    ) {
        val cacheDir = context.cacheDir

        return with(File(cacheDir, CACHE_FOLDER)) {
            if (!exists()) mkdir()

            val cacheFile = File(this, "$filename.log")

            if (cacheFile.exists()) cacheFile.delete()
            cacheFile.createNewFile()

            cacheFile.writer().use {
                it.write("")
                it.flush()
            }
        }
    }

    actual fun clearUnusedFiles(context: ApplicationContext) {
        val file = File(context.cacheDir, CACHE_FOLDER)
        file.listFiles()?.forEach { logFile ->
            if (logFile.length() == 0L) logFile.delete()
        }
    }

    private fun reduceFileContent(file: File): String {
        val queue: Queue<String> = LinkedList()
        with(file.bufferedReader()) {
            var line = readLine()
            while (line != null) {
                if (line == "" || line == " ") {
                    line = readLine()
                    continue
                }
                queue.add(line)
                if (queue.size >= 1000) queue.remove()
                line = readLine()
            }
            close()
        }

        return queue.joinToString(separator = "\n") { it }.trim()
    }
}