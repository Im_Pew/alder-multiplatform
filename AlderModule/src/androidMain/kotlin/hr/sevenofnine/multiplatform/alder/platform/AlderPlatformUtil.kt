package hr.sevenofnine.multiplatform.alder.platform

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.provider.Settings
import java.text.SimpleDateFormat
import java.util.*

actual object AlderPlatformUtil {

    @SuppressLint("HardwareIds")
    actual fun retrieveDeviceId(context: ApplicationContext): String = Settings.Secure.getString(
        context.contentResolver, Settings.Secure.ANDROID_ID
    )

    actual suspend fun isAppAlive(context: ApplicationContext): Boolean {
        val activityManager =
            context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val processInfos = activityManager.runningAppProcesses
        for (processInfo in processInfos) {
            if (processInfo.processName == context.packageName) {
                return true
            }
        }
        return false
    }

    // 00:00:00.000 01:01:1970
    actual fun currentTime(pattern: String): String {
        return SimpleDateFormat(pattern, Locale.ENGLISH).format(Date())
    }

    actual fun currentTimestamp() = System.currentTimeMillis()

    actual suspend fun isInternetConnected(context: ApplicationContext): Boolean {
        return true
    }
}