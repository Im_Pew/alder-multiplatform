package hr.sevenofnine.multiplatform.alder.platform

import android.app.Application

actual typealias ApplicationContext = Application