package hr.sevenofnine.multiplatform.alder.log

import android.content.Context
import android.content.Context.MODE_PRIVATE
import hr.sevenofnine.multiplatform.alder.Category
import hr.sevenofnine.multiplatform.alder.log.AlderUrlUtil.generateTag
import hr.sevenofnine.multiplatform.alder.platform.ApplicationContext
import io.ktor.utils.io.core.internal.*

@DangerousInternalIoApi
actual object AlderPreferences {

    actual val LINKS_PREFERENCES = "hr.dturic.personal.tdkappdemoadler.shared_preferences"

    actual val DEVICE_ID = "hr.sevenofnine.agency.alder.DEVICE_ID"
    actual val USER_ID = "hr.sevenofnine.agency.alder.USER_ID"

    actual fun saveLink(context: ApplicationContext, url: String?, alderCategory: Category?) {
        val sharedPreferences = context.getPreferences()

        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = getUserId(context) ?: "*"
        val category = alderCategory?.name ?: "*"
        val tag = generateTag(deviceId, category, userId)

        sharedPreferences.edit().putString(tag, url).apply()
    }

    actual fun getLink(context: ApplicationContext, category: Category?): String? {
        val preferences = context.getPreferences()
        val tag = generateTag(
            getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!"),
            category?.name ?: "*",
            getUserId(context) ?: "*"
        )

        return preferences.getString(tag, null)
    }

    actual fun removeLinks(context: ApplicationContext, userId: String?) {
        val preferences = context.getPreferences()
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val regex = AlderUrlUtil.getTagRegex(deviceId, userId)

        preferences.all
            .keys
            .filter { regex.matches(it) }
            .forEach { preferences.edit().remove(it).apply() }
    }

    actual fun removeLink(context: ApplicationContext, category: String, userId: String?) {
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = userId ?: "*"

        context.getPreferences()
            .edit()
            .remove(generateTag(deviceId, category, userId))
            .apply()
    }

    actual fun saveDeviceId(context: ApplicationContext, deviceId: String) {
        val sharedPreferences = context.getPreferences()

        sharedPreferences.edit()
            .putString(DEVICE_ID, deviceId)
            .apply()
    }

    actual fun removeUser(context: ApplicationContext) = context.getPreferences()
        .edit()
        .remove(USER_ID)
        .apply()

    actual fun getDeviceId(context: ApplicationContext): String? =
        context.getPreferences().getString(DEVICE_ID, null)

    actual fun saveUserId(context: ApplicationContext, userId: String) {
        val sharedPreferences = context.getPreferences()

        sharedPreferences.edit()
            .putString(USER_ID, userId)
            .apply()
    }

    actual fun getUserId(context: ApplicationContext): String? =
        context.getPreferences().getString(USER_ID, null)

    actual fun getAllCategories(context: ApplicationContext): Map<String, Any?> {
        val preferences = context.getPreferences()
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = getUserId(context)
        val regex = AlderUrlUtil.getTagRegex(deviceId, userId)

        return preferences.all
            .filterKeys { key -> regex.matches(key) && !key.contains("https?:/".toRegex()) }
    }

    private fun Context.getPreferences() =
        getSharedPreferences(LINKS_PREFERENCES, MODE_PRIVATE)
}