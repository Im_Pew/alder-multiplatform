import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("kotlinx-serialization")
    id("com.chromaticnoise.multiplatform-swiftpackage") version "2.0.3"
    id("maven-publish")
}

val projectVersion = "1.0.0"

val ktor_version = "1.6.7"
val klaxon_version = "5.5"
val serialization_version = "1.3.2"
val coroutine_version = "1.5.2"

kotlin {
    multiplatformSwiftPackage {
        packageName("AlderModule")
        swiftToolsVersion("5.3")
        targetPlatforms {
            iOS { v("12") }
        }
        outputDirectory(File(rootDir, "/"))
    }

    android {
        publishLibraryVariants("release")
    }

    ios {
        binaries {
            framework {
                baseName = "AlderModule"
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                //Ktor
                implementation("io.ktor:ktor-client-core:$ktor_version")
                implementation("io.ktor:ktor-client-serialization:$ktor_version")

                // Kotlinx Serialization
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serialization_version")

//                 Kotlin annotations
//                implementation ("androidx.annotation:annotation:1.3.0")

                // Coroutines
//                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutine_version")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.2-native-mt")

                // Gson
                implementation("com.google.code.gson:gson:2.8.9")

                // Atomic
                implementation("org.jetbrains.kotlinx:atomicfu:0.17.0")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val androidMain by getting {
            dependencies {
                // Ktor
                implementation("io.ktor:ktor-client-android:$ktor_version")

                // Android coroutines
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutine_version")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13.2")
            }
        }
        val iosMain by getting {
            dependencies {
                // Ktor
                implementation("io.ktor:ktor-client-ios:$ktor_version")
                implementation(kotlin("stdlib"))
            }
        }
        val iosTest by getting
    }
}

android {
    compileSdkVersion(30)
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdkVersion(16)
        targetSdkVersion(30)

        consumerProguardFiles("proguard-rules.pro", "consumer-rules.pro")
        proguardFiles("proguard-rules.pro")
    }
}

val packForXcode by tasks.creating(Sync::class) {
    group = "build"

    val mode = System.getenv("CONFIGURATION") ?: "DEBUG"

    val sdkName = System.getenv("SDK_NAME") ?: "iphonesimulator"
    val targetName = "ios" + if (sdkName.startsWith("iphoneos")) "Arm64" else "X64"

    val framework =
        kotlin.targets.getByName<KotlinNativeTarget>(targetName).binaries.getFramework(mode)
    val targetDir = File(buildDir, "xcode-frameworks")

    dependsOn(framework.linkTask)
    inputs.property("mode", mode)

    from({ framework.outputDirectory })
    into(targetDir)
}

tasks.getByName("build").dependsOn(packForXcode)

afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>("release") {
                this.from(components["kotlin"])

                groupId = "hr.sevenofnine.agency"
                artifactId = "alder-multiplatform"
                version = projectVersion
            }
        }
    }
}