import SwiftUI
import UIKit
import AlderModule

struct ContentView: View {
	let greet = Greeting().greeting()
    let deviceId = AlderUtil().getDeviceId(context: UIView())

	var body: some View {
        _ = getDeviceId()
        let size = initAlder()
        return Text(verbatim: String(size))
//        return Text(verbatim: Alder.shared.getIds())
	}
    
    let rawJson = "{\n" +
    "  \"config\": {\n" +
    "    \"secret_key\": \"OaRp0VxXSU40B6IS\",\n" +
    "    \"auth_url\": \"https://alder.shape404.agency/api/sas/\",\n" +
    "    \"cache_size\": 10240,\n" +
    "    \"cache_time\": 60000\n" +
    "  },\n" +
    "  \"regex\": [\n" +
    "    \"some_username_\",\n" +
    "    \"icon64\",\n" +
    "    \"iconUrl\"\n" +
    "  ]\n" +
    "}"
    
    let testJson = "{\n" +
    "  \"icon64\": \"something_to_hide\",\n" +
    "  \"ok_one\": \"this_is_fine\",\n" +
    "  \"iconUrl\": \"also_this_one_must_go\"\n" +
    "}"
    
    
    func initAlder() -> Bool {
        AlderPreferences.shared.getAllCategories(context: UIView() )
        Alder.shared.doInit(context: UIView(), rawJson: rawJson)
//        Alder.shared.loginUser(userId: "newUser")
//        Alder.shared.log(tag: "ContentView", log: "Hello iOS multiplatform", category: TestCategory())
//        Alder.shared.loginUser(userId: "testic")
//        Alder.shared.log(tag: "ContentView", log: "New line", category: nil)
        Alder.shared.logoutUser()
        Alder.shared.log(tag: "ContentView", log: "Test", category: nil)
        return true
    }
    
    func fileSize() -> Int {
        return Int(FilePlatformManager().fileSize(context: UIView(), filename: "test"))
    }
    
    func getDeviceId() -> String {
        print("app folder path is \(NSHomeDirectory())")
        let prefs = AlderPreferences()
        prefs.saveDeviceId(context: NSObject(), deviceId: deviceId)
        let id = prefs.getDeviceId(context: NSObject()) ?? "none"
        return id
    }
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
	ContentView()
	}
}

