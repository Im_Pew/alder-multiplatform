//
//  TestCategory.swift
//  AlderDemoiOS
//
//  Created by Danijel Turić on 13.01.2022..
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import AlderModule

class TestCategory: AlderModule.Category {
    var name: String = "test_category"
}
